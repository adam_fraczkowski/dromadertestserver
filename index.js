const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const basicAuth = require('express-basic-auth')
const path = require('path')
const cors = require('cors')
const helmet = require('helmet')

const url = process.env.DB || "mongodb://localhost:27017/dromader";
const PORT = process.env.PORT || 8081

mongoose.connect(url,{useNewUrlParser:true}).then(()=>{
    console.log("DB CONNECTED")
})
.catch((error)=>{
    console.error(error);
    process.exit(1);
})

const UserSchema = mongoose.Schema({
    phone:{
        type:String,
        required:true,
        unique:true
    },
    code:{
        type:String,
        required:true
    }
})

const LocationSchema = mongoose.Schema({
    phone:{
        type:String,
        required:true
    },
    
    lng:{
        type:String,
        required:true
    },

    lat:{
        type:String,
        required:true
    },

    timestamp:{
        type:Number,
        required:true
    },
    accuracy:{
        type:Number,
        required:false
    },
    speed:{
        type:Number,
        required:false
    }
})

const User = mongoose.model('User',UserSchema);
const Location = mongoose.model('Location',LocationSchema)

app.use(bodyParser.json())
app.use(cors())
//app.use(helmet())

const userName = process.env.USERNAME || "dromaderadmin"
const password = process.env.PASSWORD || "dromader2019$"

var user = {}
user[userName] = password

const authMiddleware = basicAuth({
    users:user,
    challenge: true
})

app.use('/public', express.static(path.join(__dirname, 'public')))

const codeMiddleware = (req,res,next)=>{
    if(typeof req.body.phone=='string' && typeof req.body.code == 'string') {
        User.count({phone:req.body.phone,code:req.body.code},(err,count)=>{
            if(err || count<=0) {
                res.status(401).json({"status":"Unauthorized"});
            } else {
                next()
            }
        })
    }
}

app.post("/user",authMiddleware,(req,res)=>{
    if(typeof req.body.phone=='string' && typeof req.body.code=='string') {
       var newUser = new User({phone:req.body.phone,code:req.body.code})
       newUser.save().then(()=>{
        res.status(200).json({"status":"ok"})
       })
       .catch((error)=>{
        res.status(500).json(error)
       })
    } else {
        res.status(400).json({"status":"bad data format "})
    }   
})


app.post("/user/remove/:phone",authMiddleware,(req,res)=>{
    if(typeof req.params.phone=='string') {
        User.findOneAndDelete({phone:req.params.phone}).then(()=>{
            res.status(200).json({"status":"ok"})
        }).catch((error)=>{
            res.status(500).json(error)
        })
    } else {
        res.status(400).json({"status":"bad data format "})
    }
})


app.post("/location",codeMiddleware,(req,res)=>{
    if(req.body.locations instanceof Array) {
        for(var i=0;i<req.body.locations.length;i++) {
            req.body.locations[i].phone = req.body.phone;
        }
        Location.insertMany(req.body.locations).then(()=>{
            res.status(200).json({"status":"ok"});
        })
        .catch((error)=>{
            res.status(500).json([error])
        })
    } else {
        res.status(400).json({"status":"bad data format "})
    }
})   

app.post("/login",codeMiddleware,(req,res)=>{
    res.status(200).json({"status":"ok"});
})

app.get("/location",authMiddleware,(req,res)=>{
    Location.find({}).sort({timestamp:-1}).then((docs)=>{
        res.status(200).json(docs);
    })
    .catch((error)=>{
        res.status(500).json([error])
    })
})


app.get('/user',authMiddleware,(req,res)=>{
    User.find({}).then((docs)=>{
        res.status(200).json(docs);
    })
    .catch((error)=>{
        res.status(500).json([error])
    })
})

app.get('/',authMiddleware,(req,res)=>{
    res.status(200).sendFile(__dirname+'/public/index.html');
})

app.get('/last',authMiddleware,(req,res)=>{
    Location.aggregate([
        {
            $sort:{
                   timestamp:-1
             }
        },
        {
            $group:{
                  _id:"$phone",
                  timestamp:{$first:"$timestamp"},
                  lng:{$first:"$lng"},
                  lat:{$first:"$lat"},
                  speed:{$first:"$speed"},
                  accuracy:{$first:"$accuracy"}
            }
        
        }
      ]).then((docs)=>{
        res.status(200).json(docs);
      }).catch((error)=>{
        res.status(500).json([error])
      })
})

app.listen(PORT,()=>{
    console.log("Server started at "+PORT)
})


