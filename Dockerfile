FROM node:10.15-alpine
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
RUN npm install
EXPOSE 8081
CMD ["node","index.js"]
